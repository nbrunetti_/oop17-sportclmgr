package com.biaren.utility;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Helper functions for handling dates.
 * From https://code.makery.ch/it/library/javafx-tutorial/part3/
 * @author Marco Jakob
 */
public class DateUtil {

    /** The date pattern that is used for conversion. Change as you wish. */
    private static final String DATE_DEFAULT_PATTERN = "dd/MM/yyyy";

    /** The date formatter. */
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_DEFAULT_PATTERN);
    
    /**
     * Returns the given date as a well formatted String.
     * @param date the date to be returned as a string
     * @return formatted string
     */
    public static String format(final LocalDate date) {
        if (date == null) {
            return null;
        }
        return DATE_FORMATTER.format(date);
    }
    
    /**
     * Converts a String in the format of the defined pattern
     * to a {@link LocalDate} object.
     * Returns null if the String could not be converted.
     * @param dateString the date as String
     * @return the date object or null if it could not be converted
     */
    public static LocalDate parse(final String dateString) {
        try {
            return DATE_FORMATTER.parse(dateString, LocalDate::from);
        } catch (DateTimeParseException e){
            return null;
        }
    }
    
    /**
     * Checks the String whether it is a valid date.
     * @param dateString the date as String
     * @return true if the String is a valid date
     */
    public static boolean validDate(final String dateString) {
        if (dateString == null) {
            return false;
        }
        return DateUtil.parse(dateString) != null;
    }
}
